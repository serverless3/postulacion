export const global = {
    awsConfig:{
        accessKeyId:process.env.ACCESS_KEY_ID,
        secretAccessKey:process.env.SECRET_ACCESS_KEY,
        region: process.env.REGION,
    },
    rest:{
        apiUrlBase:"https://swapi.py4e.com/api/"
    }
}