import 'reflect-metadata'
import {  Handler } from 'aws-lambda';
import serverless from 'serverless-http';
import express, { Request, Response } from 'express';
import { container } from 'tsyringe';
import FilmsController from './controllers/FilmsController';
import PeopleController from './controllers/PeopleController';
import PlanetsController from './controllers/PlanetsController';
import SpeciesController from './controllers/SpeciesController';
import StarshipsController from './controllers/StarshipsController';
import VehiclesController from './controllers/VehiclesController';
import cors from 'cors'
import * as bodyParser from "body-parser"


const app = express();
app.use(cors({origin: '*'}));
app.use(bodyParser.json())
app.use('/films', container.resolve(FilmsController).routes());
app.use('/people', container.resolve(PeopleController).routes());
app.use('/planets', container.resolve(PlanetsController).routes());
app.use('/species', container.resolve(SpeciesController).routes());
app.use('/starships', container.resolve(StarshipsController).routes());
app.use('/vehicles', container.resolve(VehiclesController).routes());

app.use((_req: Request, res: Response) => {
  res.send({ message: 'Server is running' });
});
export const handler: Handler = serverless(app);