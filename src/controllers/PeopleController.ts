
import { Request, Response, Router } from "express";
import { autoInjectable } from "tsyringe";
import PeopleService   from "../service/PeopleService";

@autoInjectable()
export default class PeopleController{
    router: Router;
    constructor(private service:PeopleService){
        this.router = Router()
    }
    async listAll(_req:Request,res: Response){
        try{
            const response=await this.service.listAll()
            return res.status(200).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async listById(_req:Request,res: Response){
        try{
            const id=_req.params.id
            const response=await this.service.listById(id)
            return res.status(200).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async add(_req:Request,res: Response){
        try{
            const adminStore=_req.body;
            const response=await this.service.add(adminStore)
            return res.status(201).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async edit(_req,res){
        try{
            const adminStore=_req.body;
            const response=await this.service.edit(adminStore)
            return res.status(200).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    routes() {
        this.router.get('/', async (_req:Request,res: Response) => { await this.listAll(_req, res)});
        this.router.get('/:id', async (_req:Request,res: Response) => { await this.listById(_req,res)});
        this.router.post('/', async (_req:Request,res: Response) => { await this.add(_req, res)});
        this.router.put('/', async (_req:Request,res: Response) => { await this.edit(_req, res)});
        return this.router;
    }

}