
import { Request, Response, Router } from "express";
import { autoInjectable } from "tsyringe";
import FilmsService   from "../service/FilmsService";

@autoInjectable()
export default class FilmsController{
    router: Router;
    constructor(private service:FilmsService){
        this.router = Router()
    }
    async listAll(_req:Request,res: Response){
        try{
            const response=await this.service.listAll()
            if(response){
                return res.status(200).json(response);
            }else{
                return res.status(404).json({detalle:"no encontrado"});
            }
            
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async listById(_req:Request,res: Response){
        try{
            const id=_req.params.id
            const response=await this.service.listById(id)
            if(response){
                return res.status(200).json(response);
            }else{
                return res.status(404).json({detalle:"no encontrado"});
            }
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async add(_req:Request,res: Response){
        try{
            const films=_req.body;
            const response=await this.service.add(films)
            return res.status(201).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    async edit(_req,res){
        try{
            const adminStore=_req.body;
            const response=await this.service.edit(adminStore)
            return res.status(200).json(response);
        }catch(e){
            console.log(e)
            return res.status(500).json({"type":"ERROR","message":e});
        }
    }
    routes() {
        this.router.get('/', async (_req:Request,res: Response) => { await this.listAll(_req, res)});
        this.router.get('/:id', async (_req:Request,res: Response) => { await this.listById(_req,res)});
        this.router.post('/', async (_req:Request,res: Response) => { await this.add(_req, res)});
        this.router.put('/', async (_req:Request,res: Response) => { await this.edit(_req, res)});
        return this.router;
    }

}