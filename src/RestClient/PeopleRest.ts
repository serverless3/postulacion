import { autoInjectable } from "tsyringe";
import fetch  from 'node-fetch';
import { global as env} from '../../global'
@autoInjectable()
export default class PeopleRest{
  listAll=async ()=>{
    const response=await fetch(env.rest.apiUrlBase+"films",{
      headers:{
          'Content-Type': 'application/json'
      }
  })
  return response.json()
}
listById=async (id)=>{
  const response=await fetch(env.rest.apiUrlBase+"films/"+id,{
    headers:{
        'Content-Type': 'application/json'
    }
  })
  return response.json()
}   
}
