import { autoInjectable } from "tsyringe";
@autoInjectable()
export default class TranslateObject{

    filmsToPelicula=(film)=>{
        try{        
            const pelicula={
                id:film.episode_id,
                titulo:film.title,
                opening_crawl:film.opening_crawl,
                director:film.director,
                productor:film.producer,
                fecha_lanzamiento:film.release_date,
                caracteres:film.characters,
                planetas:film.planets,
                naves_estelares:film.starships,
                vehiculos:film.vehicles,
                especies:film.species,
                creado:film.created,
                editado:film.edited,
              }
              return pelicula;
        }catch(e){
            throw Error(e)
        }
    }
}