import { autoInjectable } from "tsyringe";
import DynamoDataBase from "../configure/dynamoDatabase";
@autoInjectable()
export default class FilmsRepository{
    private dynamocnn:AWS.DynamoDB.DocumentClient
    constructor(dydb:DynamoDataBase){
        this.dynamocnn=dydb.dynamoClient()
    }
    listAll=async ()=>{
        const tableName="Films"
        var params = {
          TableName: tableName
        };
        return (await this.dynamocnn.scan(params).promise()).Items
    }
    listById=async (id)=>{
        const tableName="Films"
        var params = {
          TableName: tableName,
          Key:{
              id: Number(id)
          },
      
      };
         return await this.dynamocnn.get(params).promise();
    }
    
    add=async(pelicula:any)=>{        
      await this.dynamocnn.put({
        TableName: "Films",
        Item: pelicula
      }).send((err,data)=>{
        
      })
    }
    edit=async(adminStore)=>{
        const tableName="Zone"
        var params = {
          TableName: tableName
        };
        return {"hello":"hello"}//(await this.dynamocnn.scan(params).promise()).Items
    }
    listByName=async (name,zone_id)=>{
        const tableName="Zone"
        var params = {
          TableName: tableName
        };
        return {"hello":"hello"}//(await this.dynamocnn.scan(params).promise()).Items
    }    
}
