import { autoInjectable } from "tsyringe";
import PlanetsRepository  from "../repository/PlanetsRepository";
import FilmsValidator from '../validator/FilmsValidator'
@autoInjectable()
export default class PlanetsService{

    constructor(private dao:PlanetsRepository,private validator:FilmsValidator){
    }
    async listAll (){
        return await this.dao.listAll()
    }
    async listById(id){
        return await this.dao.listByDNI(id)
    }
    async add(adminStore){
        this.validator.validate(adminStore)
        await this.dao.add(adminStore)
        return {type:"SUCCESS",message:"created",result:adminStore}
    }
    async edit(adminStore){
        this.validator.validate(adminStore)
        return await this.dao.edit(adminStore)
    }

}
