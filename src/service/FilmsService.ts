import { autoInjectable } from "tsyringe";
import FilmsRepository  from "../repository/FilmsRepository";
import FilmsValidator from '../validator/FilmsValidator'
import FilmsRest from '../RestClient/FilmsRest'
import TranslateObject from '../utils/TranslateObject'
@autoInjectable()
export default class FilmsService{

    constructor(private dao:FilmsRepository,private rest:FilmsRest,private validator:FilmsValidator,private translate:TranslateObject){
    }
    async listAll (){
        const resultDB = await this.dao.listAll();
        if(!resultDB.length){
            const resultRest = await this.rest.listAll();
            if(resultRest.detail){
                return null;
            }
            const results =resultRest.results;
            for(var i=0;i<results.length;i++){
                const item=results[i]
                const pelicula=this.translate.filmsToPelicula(item)
                await this.dao.add(pelicula);
            }
            return resultRest;
        }else{
            //return {cantidad:resultDB.length,resultado:resultDB};
            return resultDB
        }
    }
    async listById(id){
        const resultDB = await this.dao.listById(id);
        if(JSON.stringify(resultDB) === JSON.stringify({})){
            const resultRest = await this.rest.listById(id);
            if(resultRest.detail){
                return null;
            }
            const pelicula=this.translate.filmsToPelicula(resultRest)
            await this.dao.add(pelicula);
            return resultRest;
        }else{
            return resultDB;
        }
    }
    async add(pelicula:any){
        this.validator.validate(pelicula)
        pelicula.creado=new Date().toDateString()
        pelicula.editado=new Date().toDateString()
        pelicula.id=(await this.dao.listAll()).length + 1
        await this.dao.add(pelicula);
        return pelicula
    }
    async edit(adminStore){
        this.validator.validate(adminStore)
        return await this.dao.edit(adminStore)
    }

}
